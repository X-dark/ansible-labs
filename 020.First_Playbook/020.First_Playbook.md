# 2 - First Playbook

This exercise makes you write your first playbook from scratch. The playbook copy a file to several hosts.

## Playbook Creation
Create a new playbook, named `copy_file.yml`. This playbook must follow the given specs:
* Run on host group `lab`
* Copy the provided file `myFile.txt` in the `/tmp` folder of each host in the group
* Must use the `copy` module

## Validation
Run the playbook and make needed changes until your playbook run as needed.

To validate, run the following command. It should return an `ok` state for each host in the `lab` group:
```bash
ansible-playbook -i ../hosts solution/validate.yml
```

There is a working playbook in `solution/solution.yml`

