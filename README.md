# Ansible Labs

This repository contains Ansible Labs used for training.

## Setup
Install Ansible and clone this repo. Customize the `hosts` file to your lab setup.

## Content
1. [Run a Playbook](010.Run_Playbook/010.Run_Playbook.md)
1. [Write your First Playbook](020.First_Playbook/020.First_Playbook.md)
1. [Include Tasks](030.Task_Inclusion/030.Task_Inclusion.md)
1. [Use Handlers](040.Handlers/040.Handlers.md)
1. [Jinja2 Template](050.Template/050.Template.md)
1. [Using Facts](060.Facts/060.Facts.md)
1. [Task Delegation](070.Delegate/070.Delegate.md)
1. [Loops](080.Loops/080.Loops.md)
1. [Conditionals](090.Conditional/090.Conditional.md)
1. [Vault](100.Vault/100.Vault.md)
1. [User Prompt](110.Prompt/110.Prompt.md)


